'\" t
.\"     Title: bootup
.\"    Author: Lennart Poettering <lennart@poettering.net>
.\" Generator: DocBook XSL Stylesheets v1.77.1 <http://docbook.sf.net/>
.\"      Date: 08/08/2012
.\"    Manual: bootup
.\"    Source: systemd
.\"  Language: English
.\"
.TH "BOOTUP" "7" "" "systemd" "bootup"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
bootup \- System bootup process
.SH "DESCRIPTION"
.PP
A number of different components are involved in the system boot\&. Immediately after power\-up, the system BIOS will do minimal hardware initialization, and hand control over to a boot loader stored on a persistent storage device\&. This boot loader will then invoke an OS kernel from disk (or the network)\&. In the Linux case this kernel now (optionally) extracts and executes an initial RAM disk image (initrd) such as
\fBdracut\fR(8)
which looks for the root file system\&. After the root file system is found and mounted the initrd hands over control to the system manager (such as
\fBsystemd\fR(1)) stored on the OS image which is then responsible for probing all remaining hardware, mounting all necessary file systems and spawning all configured services\&.
.PP
On shutdown the system manager stops all services, unmounts all file systems (detaching the storage technologies backing them), and then (optionally) jumps back into the initrd code which unmounts/detaches the root file system and the storage it resides on\&. As last step the system is powered down\&.
.PP
Additional information about the system boot process may be found in
\fBboot\fR(7)\&.
.SH "SYSTEM MANAGER BOOTUP"
.PP
At boot, the system manager on the OS image is responsible for initializing the required file systems, services and drivers that are necessary for operation of the system\&. On
\fBsystemd\fR(1)
systems this process is split up in various discrete steps which are exposed as target units\&. (See
\fBsystemd.target\fR(5)
for detailed information about target units\&.) The boot\-up process is highly parallelized so that the order in which specific target units are reached is not deterministic, but still adheres to a limited amount of ordering structure\&.
.PP
When systemd starts up the system it will activate all units that are dependencies of
default\&.target
(as well as recursively all dependencies of these dependencies)\&. Usually
default\&.target
is simply an alias of
graphical\&.target
or
multi\-user\&.target
depending on whether the system is configured for a graphical UI or only for a text console\&. To enforce minimal ordering between the units pulled in a number of well\-known target units are available, as listed on
\fBsystemd.special\fR(7)\&.
.PP
The follow chart is a structural overview of these well\-known units and their position in the boot\-up logic\&. The arrows describe which units are pulled in and ordered before which other units\&. Units near the top are started before units nearer to the bottom of the chart\&.
.sp
.if n \{\
.RS 4
.\}
.nf
local\-fs\-pre\&.target
         |
         v
(various mounts and   (various swap   (various cryptsetup
 fsck services\&.\&.\&.)     devices\&.\&.\&.)        devices\&.\&.\&.)       (various low\-level   (various low\-level
         |                  |                  |             services: udevd,     API VFS mounts:
         v                  v                  v             tmpfiles, random     mqueue, configfs,
  local\-fs\&.target      swap\&.target     cryptsetup\&.target    seed, sysctl, \&.\&.\&.)      debugfs, \&.\&.\&.)
         |                  |                  |                    |                    |
         \e__________________|_________________ | ___________________|____________________/
                                              \e|/
                                               v
                                        sysinit\&.target
                                               |
                             _________________/|\e___________________
                            /                  |                    \e
                            |                  |                    |
                            v                  |                    v
                        (various               |              rescue\&.service
                       sockets\&.\&.\&.)             |                    |
                            |                  |                    v
                            v                  |              \fIrescue\&.target\fR
                     sockets\&.target            |
                            |                  |
                            \e_________________ |
                                              \e|
                                               v
                                         basic\&.target
                                               |
            __________________________________/|                                 emergency\&.service
           /                |                  |                                         |
           |                |                  |                                         v
           v                v                  v                                 \fIemergency\&.target\fR
       display\-      (various system    (various system
   manager\&.service       services           services)
           |           required for            |
           |          graphical UIs)           v
           |                |           \fImulti\-user\&.target\fR
           |                |                  |
           \e_______________ | _________________/
                           \e|/
                            v
                    \fIgraphical\&.target\fR
.fi
.if n \{\
.RE
.\}
.PP
Target units that are commonly used as boot targets are
\fIemphasized\fR\&. These units are good choices as goal targets, for example by passing them to the
\fIsystemd\&.unit=\fR
kernel command line option (see
\fBsystemd\fR(1)) or by symlinking
default\&.target
to them\&.
.SH "SYSTEM MANAGER SHUTDOWN"
.PP
System shutdown also consists of various target units with some minimal ordering structure applied:
.sp
.if n \{\
.RS 4
.\}
.nf
                                  (conflicts with  (conflicts with
                                    all system     all file system
                                     services)     mounts, swaps,
                                         |           cryptsetup
                                         |          devices, \&.\&.\&.)
                                         |                |
                                         v                v
                                  shutdown\&.target    umount\&.target
                                         |                |
                                         \e_______   ______/
                                                 \e /
                                                  v
                                         (various low\-level
                                              services)
                                                  |
                                                  v
                                            final\&.target
                                                  |
            _____________________________________/ \e_________________________________
           /                         |                        |                      \e
           |                         |                        |                      |
           v                         v                        v                      v
systemd\-reboot\&.service   systemd\-poweroff\&.service   systemd\-halt\&.service   systemd\-kexec\&.service
           |                         |                        |                      |
           v                         v                        v                      v
    \fIreboot\&.target\fR             \fIpoweroff\&.target\fR            \fIhalt\&.target\fR           \fIkexec\&.target\fR
.fi
.if n \{\
.RE
.\}
.PP
Commonly used system shutdown targets are
\fIemphasized\fR\&.
.SH "SEE ALSO"
.PP

\fBsystemd\fR(1),
\fBboot\fR(7),
\fBsystemd.special\fR(7),
\fBsystemd.target\fR(5)
